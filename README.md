# Vario Skytraxx to CSV

When you want to export statistical data from your Vario Skytraxx 2.0
into CSV (Excel) format.

Script might work also for other Vario models as long as the _Requirements_ are met.

It extracts:

- HOGTYGLIDERTYPE - Glider name
- HOPLTPILOT - Pilot name
- HPSITSITE - Site name
- MC - ?
- MS - ?
- MSP - ?
- Dist - Distance
- GF - ?
- start_time - Start time of the activity
- end_time - End time of the activity
- date - Date of the activity

## Requirements

- Ruby >=v3.1.2
- The directory tree structure is mandatory as it's used for date extration: `input/2018/April/21/xxx.igc`
- Data are extracted from the `*.igc` files.

## Usage

1. Make an export of all the data from your Vario Skytraxx.
2. Put the exported folder tree into folder named `input` next to the `index.rb` script:
```
├── index.rb
├── input
│   ├── 2018
│   │   ├── April
│   │   │   ├── 21
│   │   │   │   ├── 84LLESC1.igc
│   │   │   │   ├── 84LLESC1.kml
│   │   │   │   ├── 84LLESC3.igc
│   │   │   │   └── 84LLESC3.kml
│   │   │   └── 8
│   │   │       ├── 848LESC3.igc
│   │   │       └── 848LESC3.kml
│   │   ├── August
│   │   │   ├── 1
│   │   │   │   ├── 87VLESC3.kml
│   │   │   │   ├── 881LESC4.igc
```
3. Run the script
```sh
ruby index.rb
```
4. Open the resulting `output.csv` in any Excel-like software:
```
HOGTYGLIDERTYPE,HOPLTPILOT,HPSITSITE,MC,MS,MSP,Dist,GF,start_time,end_time,date
Mojo 5 Glider,John Doe,Jungfraujoch,0.1,-0.1,0,0.0,1.0,10:56:03,11:27:03,21 April 2018
Mojo 5 Glider,John Doe,Jungfraujoch,7.3,-5.0,50,3.6,1.9,10:56:03,12:22:12,21 April 2018
Mojo 5 Glider,John Doe,Jungfraujoch,8.3,-4.1,53,3.7,1.5,10:56:03,14:33:08,21 April 2018
```

## Notes

From the resulting CSV it's possible to do various summaries like:

- Sum of distance
- Number of flights
- Number of unique takeoffs
- Sum of time flying