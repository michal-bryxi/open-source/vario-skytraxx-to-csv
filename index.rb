require 'csv'

INPUT_FOLDER     = './input'
OUTPUT_FILE      = './output.csv'
GPS_TRAIL_LENGTH = 37
DATA_HASH        = {HOGTYGLIDERTYPE: nil, HOPLTPILOT: nil, HPSITSITE: nil, MC: nil, MS: nil, MSP: nil, Dist: nil, GF: nil, start_time: nil, end_time: nil, date: nil}

input_files = Dir.glob("#{INPUT_FOLDER}/**/*.igc")

def get_value(input)
  return unless input

  input.split(':').last.strip
end

def get_time(input)
  hours = input[1..2]
  minutes = input[3..4]
  seconds = input[5..6]

  "#{hours}:#{minutes}:#{seconds}"
end

def get_date(input)
  tmp = input.split('/')

  year = tmp[2]
  month = tmp[3]
  day = tmp[4]

  "#{day} #{month} #{year}"
end

CSV.open(OUTPUT_FILE, 'w') do |csv|
  csv << DATA_HASH.keys

  input_files.each do |file|
    data = DATA_HASH.clone

    data[:date] = get_date(file)

    File.open(file).each_line do |orig_line|
      line = orig_line.force_encoding('iso-8859-1').encode('utf-8')

      if line.include? 'HOPLTPILOT'
        data[:HOPLTPILOT] = get_value(line)
      elsif line.include? 'HOGTYGLIDERTYPE'
        data[:HOGTYGLIDERTYPE] = get_value(line)
      elsif line.include? 'HPSITSITE'
        data[:HPSITSITE] = get_value(line)
      elsif line.include? ';Dist:'
        tmp = line.split(';')
        data[:MC] = get_value(tmp[1])
        data[:MS] = get_value(tmp[2])
        data[:MSP] = get_value(tmp[3])
        data[:Dist] = get_value(tmp[4])
        data[:GF] = get_value(tmp[5])
      elsif line.start_with?('B') && line.length == GPS_TRAIL_LENGTH
        if data[:start_time] == nil
          data[:start_time] = get_time(line)
        else
          data[:end_time] = get_time(line)
        end
      end
    end

    csv << data.values
  end
end